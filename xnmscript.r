rm(list=ls())   ## Clear Global Environment
cat("/014")     ## Clear Console

########################### Get all files from appropriate directories ###########################
setwd("xxx")

## The files are sorted in alphabetical order, on the full path if full.names = TRUE.
lockedDownFolder =  choose.dir(getwd(), "Choose a folder")
lockedDownFileList = list.files(path = lockedDownFolder)
lockedDownFilePath = list.files(path = lockedDownFolder, full.names = TRUE)

prodFolder =  choose.dir(getwd(), "Choose another folder")
prodFileList = list.files(path = prodFolder)
prodFilePath = list.files(path = prodFolder,  full.names = TRUE)

## Create Results dataframe to hold results of file comparison
mergedDf <- data.frame(File=character(),
                       RESULT=character()),# stringsAsFactors=FALSE)
mergedDf$RESULT <- 'whats'
mergedDf$File <- 'up'

#If file lists are identical  -  pickily tests file contents for exact equality.
if(identical(lockedDownFileList,prodFileList,FALSE, FALSE, FALSE, FALSE)){
  
  
  for (file in lockedDownFilePath){
    i <- 1
    x <- prodFileList[i]
    
    mergedDf$File <- x
    codeA <- readLines(lockedDownFilePath[i])
    codeB <- readLines(prodFilePath[i])
    res <- ifelse(identical(codeA,codeB,FALSE, FALSE, FALSE, FALSE), "Files match", "Files do not match, please investigate.")
    mergedDf$RESULT <- res
    
    
  }
}